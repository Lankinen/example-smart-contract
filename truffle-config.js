require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
  networks: {
    development: {
     host: "127.0.0.1",
     port: 8545,
     network_id: "*",
    },
    goerli: {
      provider: () => new HDWalletProvider(
        process.env.MNEMONIC,
        `https://goerli.infura.io/v3/${process.env.INFURA_API_KEY}`
      ),
      gas: "4500000",
      gasPrice: "10000000000",
      network_id: 5
    }
  },
  compilers: {
    solc: {
      version: "^0.8.0",
      settings: {
       optimizer: {
         enabled: false,
         runs: 200
       },
       evmVersion: "byzantium"
      }
    }
  },

  api_keys: {
    etherscan: process.env.ETHERSCAN_API_KEY
  },

  plugins: [
    'truffle-plugin-verify'
  ]
};
