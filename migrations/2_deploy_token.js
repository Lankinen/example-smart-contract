const MyToken = artifacts.require("./MyToken.sol");

module.exports = function (deployer) {
  const _name = "Test Token";
  const _symbol = "ESLL";
  deployer.deploy(MyToken, _name, _symbol);
};
